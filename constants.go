package main

import (
	"strings"
	"io/ioutil"
	)

var (
	stopWords map[string]bool
	flags map[string]string
)
func init(){
	loadSettings()
	loadStopWords()
}
//Reads a list of stop words from file STOPWORDS (one word per line), else uses list
//compiled from: http://nlp.stanford.edu/IR-book/html/htmledition/dropping-common-terms-stop-words-1.html
func loadStopWords(){
	wl, err := ioutil.ReadFile("STOPWORDS")
	if err != nil {
		stopWords = map[string]bool{"a":true, "an":true, "and":true, "are":true, "as":true, "at":true, "be":true, "by":true, "for":true, "from":true, "has":true, "he":true, "in":true, "is":true, "it":true, "its":true, "of":true, "on":true,"that":true, "the": true, "to":true, "was":true, "were":true, "will":true, "with":true}
	}else{
		stopWords=make(map[string]bool)
		lines := strings.Split(string(wl), "\n")
		for i:=0; i <len(lines)-1; i++{
			stopWords[lines[i]] = true

		}

	}
	return 
}

//Read SETTINGS file from disk, if not found initalize to defaults.
func loadSettings(){
	flags = make(map[string]string)
	sl, err := ioutil.ReadFile("SETTINGS")
	if err!= nil{
			flags["maxCount"] = "25"
			flags["useThresholdClustering"] = "false"
	}else{
		lines := strings.Split(string(sl),"\n")
		for i:=0; i < len(lines)-1; i++{
			kv := strings.Split(lines[i], ",")
			flags[kv[0]]= kv[1]
		}
	}
}