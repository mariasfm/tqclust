package main

import ("time"
		"errors"
		)


type tweetVector struct{
	text string
	screenName string
	termFreq map[string]float64
	vector []float64
	Recency time.Time
	querySimilarity float64
	id int
	}

func (tv tweetVector) String()string{
	s := tv.screenName + "::" + tv.text + "\n" + tv.Recency.Format(time.RFC1123) 
	return s 
}
func (tv tweetVector) setTF(tf map[string]float64){
		tv.termFreq = tf
}
func (tv tweetVector) Equals(tv2 tweetVector) bool{
	return tv.text == tv2.text && tv.screenName == tv2.screenName && tv.Recency.Equal(tv2.Recency)
}
func (tv *tweetVector) ptrEquals(tv2 *tweetVector) bool{
	return tv.text == tv2.text && tv.screenName == tv2.screenName && tv.Recency.Equal(tv2.Recency)
}

type vectorList []*tweetVector

func (t vectorList) Len() int { return len(t)}
func (t vectorList) Swap(i,j int) { t[i], t[j] = t[j], t[i]}
func (t vectorList) Less(i, j int) bool { return t[i].Recency.Before(t[j].Recency)}

func (t vectorList) findBestTweet() (*tweetVector, error){
	var tmp *tweetVector
	msim := -2.0
	index := -1
	for i:=0; i < len(t); i++{
		if t[i].querySimilarity > msim{
			msim = t[i].querySimilarity
			index = i
		}
	}
	if index >= 0{ return t[index], nil
		}else{
			return tmp, errors.New("Can't find best tweet in empty vector list")}

}

type vectorListSim []tweetVector

func (t vectorListSim) Len() int { return len(t)}
func (t vectorListSim) Swap(i,j int) { t[i], t[j] = t[j], t[i]}
func (t vectorListSim) Less(i, j int) bool { return t[i].querySimilarity <t[j].querySimilarity}