package main

type Cluster struct{
	center []float64
	memSum []float64
	id int
	members []*tweetVector
	maxQuerySim     float64
	size int
}

//recompute the center
func (tc *Cluster) updateCluster(newMem *tweetVector){
	tc.size+=1
	tc.members = append(tc.members, newMem)
	if tc.size == 1{
		tc.memSum = newMem.vector
		tc.center = newMem.vector
		tc.maxQuerySim = newMem.querySimilarity
	} else{
		for j:=0; j < len(tc.memSum); j++{
			tc.memSum[j] += newMem.vector[j]
			tc.center[j] = tc.memSum[j] / float64(tc.size)
		}
	if tc.maxQuerySim < newMem.querySimilarity{tc.maxQuerySim = newMem.querySimilarity}
	}


}
type clusterList []Cluster

func (c clusterList) Len() int { return len(c)}
func (c clusterList) Swap(i,j int) { c[i], c[j] = c[j], c[i]}
func (c clusterList) Less(i, j int) bool { return c[i].maxQuerySim < c[j].maxQuerySim}
