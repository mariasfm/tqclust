package main
import ("fmt"
		"bufio"
    	"os"
    	"strings"
    	"github.com/kurrik/oauth1a"
		"github.com/kurrik/twittergo"
		"io/ioutil"
		"net/http"
		"net/url"
		"math/rand"
		"sort"
		"time"
		"errors"
		"math"
		)

/*Loads credentials from file. Lines 0 and 1 are the consumerKey and secret for the app, while lines 
	2 and 3 are the key and secret for the authorized token
  Function reused from twittergo-example
*/
func LoadCredentials() (client *twittergo.Client, err error) {
	credentials, err := ioutil.ReadFile("CREDENTIALS")
	if err != nil {
		return
	}
	lines := strings.Split(string(credentials), "\n")
	config := &oauth1a.ClientConfig{
	ConsumerKey:    lines[0],
	ConsumerSecret: lines[1],
	}
	user := oauth1a.NewAuthorizedConfig(lines[2], lines[3])
	client = twittergo.NewClient(config, user)
	return
}

func main() {
	var (
		err     error
		twitterClient  *twittergo.Client
		)
	//load credentials here. 
	twitterClient, err = LoadCredentials()
	if err != nil {
		fmt.Printf("Error reading CREDENTIALS file: %v\n", err)
		os.Exit(1)
		}
	if len(os.Args)>1 {
		if len(os.Args) !=2{
			fmt.Println("Usage: tqclust <queryStr>")
		}else{
			queryStr := os.Args[1]
			fmt.Println("Query %v",queryStr)
			process(twitterClient, queryStr)
		}

	}else{
		reader := bufio.NewReader(os.Stdin)
		continuePrompt := true
		for continuePrompt{
			fmt.Print("Enter query and press Enter, Press Enter to exit: ")
    		queryStr, _ := reader.ReadString('\n')
    		queryStr = strings.Trim(queryStr,"\n")
    		continuePrompt = (queryStr != "")
    		if !continuePrompt{
    			fmt.Println("Exiting ...")
    			os.Exit(0)
    		}
    		process(twitterClient, queryStr)
    	    	
		}
	}
	
}
func process(twitterClient *twittergo.Client, queryStr string){
	searchRes := search(queryStr, twitterClient)
    	tvectors, _ := vectorize(searchRes, queryStr)  
    	if flags["useThresholdClustering"]=="true"{
    		tweetClusters := computeClusters(tvectors,0.85)
    		clusterPrint(tweetClusters)
    	}else{
    		tweetClusters := computeClustersDensity(tvectors,false)
    		clusterPrint(tweetClusters)
    	}

}
/*
	Print clusters in sorted order of their query similarity.
	For each cluster:
		print best tweet first
		print others in order of recency 
*/
func clusterPrint(clusters []Cluster){
	sort.Sort(sort.Reverse(clusterList(clusters)))
	for cl:=0; cl < len(clusters); cl++{
		//find the best
		clmem := vectorList(clusters[cl].members)
		if len(clmem) > 0{
			fmt.Printf("\n\nGroup %v, Cluster %v\n", cl, clusters[cl].id)
			bt, err:= clmem.findBestTweet()
			errorCheck(err, "Cannot find best in cluster")
			tweetPrint(bt,true,0)
			sort.Sort(sort.Reverse(clmem))
			tnum:=1
			for j:=0; j < len(clmem); j++{
				if ! clmem[j].ptrEquals(bt){
					tweetPrint(clmem[j],false, tnum)
					tnum+=1
				}
			}
		}
	}
}

func tweetPrint(t *tweetVector, isFirst bool, tweetNum int){
	if isFirst{
		fmt.Printf("Best: %s, %v, %v \n", t.screenName, t.text, t.Recency.Format(time.RFC1123) )

		}else{
			fmt.Printf("(%v): %v, %v, %v \n", tweetNum, t.screenName, t.text, t.Recency.Format(time.RFC1123) )
		}
	
}
//convert twittergo statuses or tweets into tweet vectors using tf-idf
func vectorize(results *twittergo.SearchResults, queryString string) ([]tweetVector, []float64){
	var tvs []tweetVector
	var allD  [] map[string]float64

	for i, tweet := range results.Statuses() {
		content:= tweet.Text()
		tv:= tweetVector{id:i,Recency:tweet.CreatedAt(),text:content, screenName:tweet.User().ScreenName()}
		tmp :=computeTF(content, true)
		tv.termFreq = tmp  
		tvs = append(tvs, tv)
		allD = append(allD, tv.termFreq)
	}

	idf, vocab := computeIDF(allD)
	qTF := computeTF(queryString,true)
	qVector := computeVector(idf, qTF, vocab)
	//compute vectors for each tweet as well as their similarity to the query
	for i :=0; i < len(tvs); i++{
		tvs[i].vector = computeVector(idf, tvs[i].termFreq, vocab)
		tmp, err:=cosineSimilarity(qVector, tvs[i].vector)
		errorCheck(err, "Error in cosine similarity in vectorize")
		tvs[i].querySimilarity = tmp		
	}

	
	return tvs, qVector
}
//search twitter for the query
func search(queryString string, client *twittergo.Client) *twittergo.SearchResults {

	query := url.Values{}
	query.Set("q", queryString)
	maxCount := flags["maxCount"]
	if len(maxCount) == 0{
		errorCheck(errors.New("maxCount is zero or not set"), "")}
	query.Set("count", maxCount)
	url := fmt.Sprintf("/1.1/search/tweets.json?%v", query.Encode())
	request, err := http.NewRequest("GET", url, nil)
	errorCheck(err, "Could not parse request")
		
	searchResponse, err := client.SendRequest(request)
	errorCheck(err, "Error sending search request:")
	//parse results
	results := &twittergo.SearchResults{}
	err = searchResponse.Parse(results)
	errorCheck(err, "Problem parsing search response:")
	return results
}
 
//Add rate limit error casting.   
func errorCheck(err error, customResponse string){
	if err!= nil{
		fmt.Println(customResponse + "%v", err)
		os.Exit(1)
	}
}


/*
Cluster the tweet vecs using a Chinese Restaurant Process
	For each vector:
        Compute it's score vs each existing cluster, and choose the best scoring cluster
        If best_score >= 1 / (1 + num_clusters) or random() >= 1 / (1 + num_clusters):
            Add to best cluster
        Else:
            Create a new cluster
Note: Results for small number of vectors might be better without added randomness of "random() >= 1 / (1 + num_clusters)"
Allows non-parametric clustering; roughly approximates a density based clustering approach

*/
func computeClustersDensity(tvs []tweetVector, addRandomness bool) []Cluster{
	var computedClusters []Cluster
	numClust :=0
	newClusterProb := 1.0
	for i:=0; i < len(tvs); i++{
		cVector := tvs[i]
		addNewCluster := false
		if len(computedClusters) == 0 {
			addNewCluster = true
			}else{
				//compute similarities
				bestSim := -2.0
				bestClustIndex := -1
				for j:=0; j < len(computedClusters); j++{
					cCluster := computedClusters[j]
					//find the best cluster
					cSim, err := cosineSimilarity(cVector.vector, cCluster.center)
					errorCheck(err, "Error in cosine similarity under compute Cluster density")
					if cSim > bestSim{
						bestSim = cSim
						bestClustIndex = j
					}
				}
				//determine whether to add to the best cluster or start a new one
				toss:= rand.Float64()
				if !addRandomness{toss=-1}
				if bestSim >= newClusterProb || toss >= newClusterProb{
					//add the current vector to its members and update the center or 'sum' of the best cluster

					computedClusters[bestClustIndex].updateCluster(&cVector)
				}else{
					addNewCluster = true
				}
			}
		if addNewCluster{
			c := Cluster{id:numClust}
			c.updateCluster(&cVector)
			computedClusters = append(computedClusters,c)
			numClust+=1
			newClusterProb = 1/ (1+ math.Log10(float64(len(computedClusters))))
		}
	}
	return computedClusters

}

/*
Cluster the tweet vecs using a threshold, allowing a high similarity cutoff.
*/
func computeClusters(tvs []tweetVector, threshold float64) []Cluster{
	var computedClusters []Cluster
	numClust :=0
	//tvs:=vectorListSim(tvsV)
	//sort.Sort(tvs)
	for i:=0; i < len(tvs); i++{
		cVector := tvs[i]
		addNewCluster := false
		if len(computedClusters) == 0 {
			addNewCluster = true
			}else{
				//compute similarities
				bestSim := -2.0
				bestClustIndex := -1
				for j:=0; j < len(computedClusters); j++{
					cCluster := computedClusters[j]
					//find the best cluster
					cSim, err := cosineSimilarity(cVector.vector, cCluster.center)
					errorCheck(err, "Error finding cosine sim in compute Cluster")
					if cSim > bestSim{
						bestSim = cSim
						bestClustIndex = j
					}
				}
				//determine whether to add to the best cluster or start a new one
				if bestSim >= threshold{
					//add the current vector to its members and update the center or 'sum' of the best cluster
					computedClusters[bestClustIndex].updateCluster(&cVector)
				}else{
					addNewCluster = true
				}
			}
		if addNewCluster{
			c := Cluster{id:numClust}
			c.updateCluster(&cVector)
			computedClusters = append(computedClusters,c)
			numClust+=1
		}
	}
	return computedClusters

}
