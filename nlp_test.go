package main
import ("testing"
	"fmt"
	"sort"
	)

func TestMagnitude(t *testing.T){
	v1 := []float64{0,0,0}
	v2:= []float64{0,2, 2, 2, 2}
	v3:= []float64{}
	fmt.Println("Vector Magnitude Test")
	if magnitude(v1) != 0{t.Error("Incorrect magnitude for zero vector") }
	if magnitude(v2) != 4 { t.Error("Incorrect magnitude")}
	if magnitude(v3) != 0 {t.Error("Incorrect magnitude for empty vector")}

}
func TestCsm(t *testing.T){
	v1 := []float64{0,2, 2, 2, 2}
	v2 := []float64{0,2, 2, 2, 2}
	fmt.Println("Cosine Similarity Test")
	cs, err := cosineSimilarity(v1, v2)
	if cs < 0.99 { t.Error("Incorrect cosine similarity ",cs)}
	cs, err = cosineSimilarity(v1, []float64{0,0,0,0,0})
	if cs != 0 { t.Error("Incorrect cosine similarity with a zero vector ", cs)}
	cs, err = cosineSimilarity([]float64{}, []float64 {})
	if err == nil { t.Error("Incorrect cosine similarity with an empty vector ", cs)}


}

func TestDotProduct(t *testing.T){
	v1 := []float64{0,2, 2, 2, 2}
	v2 := []float64{0,2, 2, 2, 2}
	fmt.Println("Dot Product Test")
	dp, err := dotProduct(v1, v2)
	if dp!= 16 { t.Error("Incorrect value for dot product ", dp)}

	dp, err = dotProduct(v1, []float64{0,2,3})
	if err == nil { t.Error("Incorrect dot product for variable dimension vectors ",dp)}

	dp, err = dotProduct([]float64{}, []float64{})
	if err != nil || dp != 0 { t.Error("Incorrect dot product for empty vectors ", dp)}

}

func TestComputeTF(t *testing.T){
	fmt.Println("ComputeTF test")
	tf := computeTF("",true)
	for k, v:= range(tf){ t.Error("Found key in empty tf ", k, v)}

	tf = computeTF("a sample text Sample book", true)
	if tf["a"] > 0 {t.Error("Stop word counted", tf["a"])}
	if tf["sample"] != 0.5 { t.Error("Incorrect frequency", tf["sample"])}

	tf = computeTF("a sample text Sample book", false)
	if tf["a"] == 0 {t.Error("Stop word not counted", tf["a"])}

}


func TestComputeIDF(t *testing.T){
	fmt.Println("Compute IDF test")
	tf1:= computeTF("",true)
	tf2:= computeTF("",true)

	idf, keys := computeIDF([]map[string]float64{tf1, tf2})
	if len(keys) > 0{ t.Error("Found keys in empty IDF ", keys)}

	tf1 = computeTF("this is a string", true)
	tf2 = computeTF("this is a string", true)

	//all values in idf should be one.
	idf, keys = computeIDF( []map[string]float64 {tf1, tf2} )

	if idf["this"] != 1 || idf["string"] != 1 {t.Error("Incorrect idf ", idf["this"], idf["string"])}
	if ! sort.StringsAreSorted(keys) { t.Error("Vocab Index not sorted", keys)}
}