package main
import ("testing"
		"fmt"
		)

func TestComputeClusters(t *testing.T){
	fmt.Println("Test compute cluster")
	q:= "kmeans clustering"

	s1:= "kmeans is a clustering algorithm"
	s2:= "kmeans is a clustering algorithm #KMeans"

	s3:= "dbscan and restaurant processes"
	s4:= "dbscan and restaurant processes also"

	s1tf := computeTF(s1, true)
	s2tf := computeTF(s2, true)
	s3tf := computeTF(s3, true)
	s4tf := computeTF(s4, true)
	qtf := computeTF(q, true)
	idf, vocab := computeIDF([]map[string]float64 {s1tf, s2tf, s3tf, s4tf})

	qv := computeVector(idf, qtf, vocab )
	s1v := computeVector(idf, s1tf, vocab)
	s2v := computeVector(idf, s2tf, vocab)
	s3v := computeVector(idf, s3tf, vocab)
	s4v := computeVector(idf, s4tf, vocab)
	q1s, _ := cosineSimilarity(s1v, qv)
	q2s, _ := cosineSimilarity(s2v, qv)
	q3s, _ := cosineSimilarity(s3v, qv)
	q4s, _ := cosineSimilarity(s4v, qv)

	tv1:= tweetVector{text:s1, termFreq:s1tf, vector:s1v,querySimilarity:q1s}
	tv2:= tweetVector{text:s2, termFreq:s2tf, vector:s2v,querySimilarity:q2s}
	tv3:= tweetVector{text:s3, termFreq:s3tf, vector:s3v,querySimilarity:q3s}
	tv4:= tweetVector{text:s4, termFreq:s4tf, vector:s4v,querySimilarity:q4s}

	threshold := 1.0

	clist := computeClusters([]tweetVector{tv1, tv2, tv3, tv4}, threshold)
	if len(clist) != 4{ t.Error("Incorrect cluster number for exact similarity ", len(clist))}

	clist = computeClusters([]tweetVector{tv1, tv2, tv3, tv4}, -1.0)

	if len(clist) != 1 { t.Error("Incorrect cluster number for no similarity threshold ", len(clist))}

}