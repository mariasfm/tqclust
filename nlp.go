package main

import (//"fmt"
	"strings"
	"math"
	"sort"
	"errors"
	)
//Bug(msmeerza) does not strip the text of punctuation
//Do not filter out stop words to support phrase search in query.
func computeTF(text string, useStopWords bool)map[string]float64{
	words := strings.Fields(strings.ToLower(text))
	tf := make(map[string]float64)
	n:=0.0
	for i:=0; i <len(words); i++{
		if !(useStopWords && isStopWord(words[i])){
			tf[words[i]] +=1
			n+=1
		}
	}
	
	for word, freq := range tf{
		tf[word] = freq/n
	}
	return tf
}

func isStopWord(word string)bool{
	return stopWords[word]
}
//compute IDF for all terms seen
func computeIDF(documentTFs []map[string]float64) (map[string]float64,[]string){

	corpusIDF := make(map[string]float64)
	corpusLength:=float64(len(documentTFs))
	for docNum:=0; docNum < len(documentTFs); docNum++{
		for term, _ := range documentTFs[docNum]{
			corpusIDF[term] += 1
		}
	}
	
	for term, freq := range corpusIDF{
		idf := 1.0 + math.Log(corpusLength/ freq)
		corpusIDF[term] = idf
	}
	vocabIndex := orderVocab(corpusIDF)
	return corpusIDF, vocabIndex
}

func orderVocab(corpusIDF map[string]float64) []string{
	var keys []string
	for k, _ := range corpusIDF{
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

func computeVector(idfMap map[string]float64, tfMap map[string]float64, vocabIndex []string) []float64{
	var vector []float64
	for i:=0; i < len(vocabIndex); i++{
		term := vocabIndex[i]
		vector = append(vector, idfMap[term] * tfMap[term])

	}

	return vector
}


//return magnitude of a vector
func magnitude(v1 []float64) float64{
	var sum float64 = 0
	for i := 0; i < len(v1); i++ {
		sum+= (v1[i]*v1[i])
	}
	return math.Sqrt(sum)
}

//the calling function should ensure vector lengths are the same.
func dotProduct(v1 []float64, v2 []float64) (float64, error){
	if len(v1) != len(v2){
		return 0, errors.New("Length of input for dot product not the same")
	}
	dp:= 0.0	
	for i:=0; i < len(v1); i++{
		dp += (v1[i] * v2[i])
		}
	return dp, nil

}

func cosineSimilarity(v1 []float64, v2 []float64) (float64, error){
	if len(v1) == 0 || len(v2) == 0 {
		return 0, errors.New("Cannot find cosine similarity in 0 vectors")
	}
	dp, err := dotProduct(v1, v2)
	if err != nil { return 0, err}
	m1 := magnitude(v1)
	m2:= magnitude(v2)
	//avoid division by zero. 
	if m1 == 0{m1 = 1}
	if m2 == 0 { m2 = 1}
	return  dp/ (m1*m2), nil

}